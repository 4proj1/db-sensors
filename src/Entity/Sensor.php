<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SensorRepository")
 */
class Sensor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $way;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tile", inversedBy="sensors")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SensorRecord", mappedBy="sensor", orphanRemoval=true)
     */
    private $sensorRecords;

    public function __construct()
    {
        $this->sensorRecords = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getWay(): ?string
    {
        return $this->way;
    }

    public function setWay(string $way): self
    {
        $this->way = $way;

        return $this;
    }

    public function getTile(): ?Tile
    {
        return $this->tile;
    }

    public function setTile(?Tile $tile): self
    {
        $this->tile = $tile;

        return $this;
    }

    /**
     * @return Collection|SensorRecord[]
     */
    public function getSensorRecords(): Collection
    {
        return $this->sensorRecords;
    }

    public function addSensorRecord(SensorRecord $sensorRecord): self
    {
        if (!$this->sensorRecords->contains($sensorRecord)) {
            $this->sensorRecords[] = $sensorRecord;
            $sensorRecord->setSensor($this);
        }

        return $this;
    }

    public function removeSensorRecord(SensorRecord $sensorRecord): self
    {
        if ($this->sensorRecords->contains($sensorRecord)) {
            $this->sensorRecords->removeElement($sensorRecord);
            // set the owning side to null (unless already changed)
            if ($sensorRecord->getSensor() === $this) {
                $sensorRecord->setSensor(null);
            }
        }

        return $this;
    }
}
