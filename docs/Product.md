# Product

This table represent a product.
It must be on a shelf.


## Columns

### id

Type : int(11)  
Definition : Identifier of a tile.  
Example of values : 12345  

### name

Type : varchar(255)  
Definition : Name of the product.  
Example of values : "Pie"   

### weight

Type : int(11)  
Definition : Weight of the product, useful for weight sensors.  
Example of values : 12  

### tile_id

Type : int(11)  
Definition : Id of the tile where the product is.  
Example of values : 12345  

## Constraints

### PRIMARY KEY(id)

### FOREIGN KEY(tile_id, Tile)
Cf. [Tile documentation](Tile.md)