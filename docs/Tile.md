# Tile

This table represent a part of a shop.
It can be a wall, a path or a shelf.


## Columns

### id

Type : int(11)  
Definition : Identifier of a tile.  
Example of values : 12345  

### x

Type : int(11)  
Definition : Absciss of the tile in the shop.  
Example of values : 12  

### y

Type : int(11)  
Definition : Ordinate of the tile in the shop.  
Example of values : 34  

### type

Type : varchar(255)  
Definition : Type of the tile. Can be "path", "shelf" or "wall".  
Example of values : "path"   

## Constraints

### PRIMARY KEY(id)