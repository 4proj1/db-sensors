# Sensor

It represents a sensor.
It can me a weight or an echo sensor for example.

## Columns

### id

Type : int(11)  
Definition : Identifier of a sensor.  
Example of values : 12345  

### type

Type : varchar(255)  
Definition : Type of the sensor. Can be "echo" or "weight".  
Example of values : "echo"  

### way

Type : varchar(255)  
Definition : Way of the sensor. Only useful for echo sensors. Can be "top", "bottom", "right", "left" or be null if not applicable.  
Example of values : "top"  

### tile_id

Type : int(11)  
Definition : Id of the tile where the sensor is.  
Example of values : 12345  

## Constraints

### PRIMARY KEY(id)

### FOREIGN KEY(tile_id, Tile)
Cf. [Tile documentation](Tile.md)