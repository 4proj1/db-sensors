deploy-dev:
	docker container prune
	docker volume prune
	docker-compose up

migration:
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console doctrine:migrations:migration
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console make:migration

push:
	docker build . -t db-sensors:0.3
	docker image tag db-sensors:0.3 c-est.party:5000/db-sensors:0.3
	docker push c-est.party:5000/db-sensors:0.3